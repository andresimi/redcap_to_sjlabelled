---
title: "Apply redcap_to_sjlabelled function"
author: "André Simioni"
date: "26/07/2020"
output: html_document 
---

# Settings
```{r}
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/redcap_to_sjlabelled/")
```

# Functiom
Load **redcap_to_sjlabelled** function
```{r}
source("redcap_to_sjlabelled.r", echo = T)
```

# Apply funtion
Apply the function and transcode Redcap script to a *sjlabelled* script.
```{r}
# Screening
redcap_to_sjlabelled(data = "~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Raw/w0:2/bhrc_unlabelled.rds",
                     new_data = "~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Raw/w0:2/bhrc_labelled.rds",
                     script = "~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Raw/w0:2/bhrc_scr_raw.r",
                     new_script = "~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Raw/w0:2/bhrc_sj.r")
```

# Run the sjlabelled script
Run the script and create a *.rds* file.
```{r}
source("~/Documentos/pCloud/Projetos/Analises/bhrc/bhrc_data/Raw/w0:2/bhrc_sj.r", echo=T)
```

