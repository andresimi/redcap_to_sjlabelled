# Settings 
library(rio); library(tidyverse); library(rlang)

# Function
#data <- "bhrc_w1_raw.csv"
#script <- "bhrc_w1_raw.r"
#new_script <- "bhrc_w1_sj.r"

redcap_to_sjlabelled <- function(data, new_data, script, new_script) {
  # Read in the data and script file.
  data <- enexpr(data)
  script <- read_lines(script)
  
  # Every line in the script file...
  ## that begins with the label function
  redcap_label <- str_subset(script, "^label\\(")
  redcap_label2 <- str_replace(redcap_label, "label", "set_label")
  
  ## that uses the factor() function
  redcap_factor <- str_subset(script, "factor\\(")
  fac <- redcap_factor %>% str_extract_all("\\\"[[:print:]]*?\\\"", simplify = F)
  
  ## that uses the levels() function
  redcap_levels <- str_subset(script, "levels\\(")
  lev <- str_extract_all(redcap_levels, "\\\"[[:print:]]*?\\\"", simplify = F)
  var <- str_extract(redcap_levels, "\\w+\\$\\w+")
  labels <- map2(lev, fac, ~paste(..1, "=", ..2) %>% paste(collapse = ", "))
  setlabel <- map2(var, labels, ~paste0(.x," <- set_labels(", .x, ", labels = c(", .y, "))"))
  
  # Mount script sjlabelled
  clear <- "rm(list=ls()); graphics.off()"
  library <- "library(rio); library(tidyverse); library(sjmisc); library(sjlabelled)"
  read <- paste0("data <- import(file ='", data,"', setclass='tibble')")
  #remove_empty <- "data <- remove_empty_cols(data)"
  export <- paste0("export(data,'", new_data, "')")
  
  # Reorder script
  script_reorder <- c("# Clear existing data and graphics", clear, 
                      "\n# Load libraries", library,
                      "\n# Read data", read,
                      "\n# Set variable labels", redcap_label2, 
                      "\n# Set value labels", setlabel, 
                      #"\n# Remove empty cols", remove_empty,
                      "\n# Export", export) %>%
    as.character()
  
  # Save script
  write_lines(script_reorder, path = new_script)
}
