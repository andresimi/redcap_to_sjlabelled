# Redcap and Labelled data in R  
One of the difficulties when **exporting data from Redcap to R** is that Redcap duplicates the factor variables, leaving:
1. The first copy with the variable numeric code; 
2. The second copy (with suffix ".factor") with the value labels.

Not being possible to have the numeric code and the value label in the same variable.  
This function transcodes ".r" syntax exported from Redcap, which uses **Hmisc** package as standard, to a syntax using the **sjlabelled** package, witch permits to set value labels to numeric variables, not requiring to duplicate the factor variables.

# Labelled Data and the sjlabelled-Package
You can find a wiki of how to work with labelled data with the **sjlabelled** package here: [https://strengejacke.github.io/sjlabelled/index.html](https://strengejacke.github.io/sjlabelled/articles/intro_sjlabelled.html).

# Source projects:
- Redcap: [https://www.project-redcap.org/](https://www.project-redcap.org/);  
- sjlabelled R package: [https://strengejacke.github.io/sjlabelled/index.html](https://strengejacke.github.io/sjlabelled/index.html)  
Lüdecke D (2020). _sjlabelled: Labelled Data Utility Functions (Version 1.1.6)_. doi: 10.5281/zenodo.1249215 (URL:
https://doi.org/10.5281/zenodo.1249215), <URL: https://CRAN.R-project.org/package=sjlabelled>.